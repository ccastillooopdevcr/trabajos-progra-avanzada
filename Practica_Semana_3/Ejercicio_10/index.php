<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  </head>
  <body>

    <div class="container" style="margin-top: 25px;">
      <div class="row">
          <div class="col">
          <form action="result.php" method="post">
            <div class="form-group">
              <label>Nombre</label>
              <input name="name" type="text" class="form-control">
            </div>
            <div class="form-group">
              <label>Acerca de ...</label>
              <textarea name="about" class="form-control" rows="3"></textarea>
            </div>
            <div class="form-group">
              <label>Genero</label>
              <select class="form-control" name="genre">
                <option value="Hombre">Hombre</option>
                <option value="Mujer">Mujer</option>
              </select>
            </div>
            <button type="submit" class="btn btn-primary">Guardar</button>
          </form>
          </div>
        </div>
      </div>

  </body>
</html>
