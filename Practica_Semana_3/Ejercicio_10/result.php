<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  </head>
  <body>

    <div class="container" style="margin-top: 25px;">
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Campo</th>
                    <th scope="col">Valor</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Nombre</td>
                        <td><?php echo $_POST["name"]; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Acerca</td>
                        <td><?php echo $_POST["about"]; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Genero</td>
                        <td><?php echo $_POST["genre"]; ?></td>
                    </tr>
                </tbody>
            </table>        
        </div>
    </div>

  </body>
</html>