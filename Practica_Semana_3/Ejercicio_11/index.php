<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  </head>
  <body>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <div class="container" style="margin-top: 25px;">
      <div class="row">
          <div class="col">
          <form id="calculator" action="calculator.php" method="post">
            <div class="form-group">
              <label>Operador 1</label>
              <input name="operator_1" type="number" class="form-control">
            </div>
            <div class="form-group">
              <label>Operador 2</label>
              <input name="operator_2" type="number" class="form-control">
            </div>
            <div class="form-group">
              <label>Operacion</label>
              <select class="form-control" name="operator">
                <option value="+">Suma</option>
                <option value="-">Resta</option>
                <option value="*">Muktiplicacion</option>
                <option value="/">Division</option>
              </select>
            </div>
            <button type="submit" class="btn btn-primary">Calcular</button>
            <br>
            <br>
            <p class="result"> Resultado: 0 </p>
          </form>
          </div>
        </div>
      </div>

      <script>
      
      /* Attach a submit handler to the form */
      $("#calculator").submit(function(event) {
          var ajaxRequest;

          event.preventDefault();

          var resultContainer = $(".result");

          resultContainer.text('');

          var values = $(this).serialize();

            ajaxRequest= jQuery.ajax({
                  url: "calculator.php",
                  type: "post",
                  data: values
              });

          ajaxRequest.done(function (response, textStatus, jqXHR){
                resultContainer.text('Resultado: ' + response.result);
          });

          ajaxRequest.fail(function (){
            // show error
            resultContainer.text('There is error while submit');
          });

        });

      </script>

  </body>
</html>
